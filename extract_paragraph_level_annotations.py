__author__ = "Giovanni Da San Martino"
__copyright__ = "Copyright 2022"
__credits__ = ["Giovanni Da San Martino"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Giovanni Da San Martino"
__email__ = "giovanni.dasanmartino@unipd.it"
__status__ = "Beta"

import sys
import codecs
import re
import os.path
import argparse
import src.annotation as an
import src.article_annotations as aa


def extract_article_id_from_filename(filename:str)->str:

    regex = re.compile("article([0-9]+).*")
    article_id = regex.match(os.path.basename(filename)).group(1)
    return article_id


def main(args):

    span_file = args.spans_file
    article_file = args.article_file
    output_file = args.output_file_name
    ann_shift = int(args.ann_shit_per_line)

    annotations = aa.Articles_annotations()
    annotations.load_article_annotations_from_csv_file(span_file, an.Annotation)
    if len(annotations)==0:
        annotations.add_article_id(extract_article_id_from_filename(span_file))
    annotations.sort_spans()
    annotations.has_overlapping_spans(False, True)

    with open(article_file, "r", newline='') as f:
        article_content = f.read()

    indices, labels, texts = annotations.extract_paragraph_level_annotations(article_content, ann_shift, True, True)
    #print("%d,%d,%d\n"%(len(indices), len(labels), len(texts)))
    #print(indices); print(labels); print(texts);
    
    article_id = annotations.get_article_id()
    with codecs.open(output_file, "w", encoding="utf8") as fout1:
        with codecs.open(output_file + ".template", "w", encoding="utf8") as fout2:
            for i in range(len(indices)):
                fout1.write("%s\t%d\t%s\n"%(article_id, indices[i], labels[i]))
                fout2.write("%s\t%d\t%s"%(article_id, indices[i], texts[i]))
    print("Output written to files: %s, %s"%(output_file, output_file + ".template"))
    #for ann in annotations:
    #    print(ann)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description="Extract paragraph level annotations\n" + "Example: python extract_paragraph_level_annotations.py -t data/article727497152.txt -s data/article727497152.labels -n data/article727497152.in")
    parser.add_argument('-t', '--text-file', dest='article_file', required=True,
        help="text document file without added spaces and transformed chars")
    parser.add_argument('-s', '--spans-file', dest='spans_file', required=True, 
        help="file with the list of annotations referring to text-file.")
    parser.add_argument('-n', '--output-annotation-file-name',
        dest='output_file_name', required=False, default=None, help="file name which paragraph-level annotations will be saved to")
    parser.add_argument('-m', '--annotation-shift',
        dest='ann_shit_per_line', required=False, default=0, help="shift each annotation of this value every after reading each input line")

    main(parser.parse_args())
